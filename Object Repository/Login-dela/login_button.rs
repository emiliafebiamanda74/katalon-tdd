<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>login_button</name>
   <tag></tag>
   <elementGuidId>5f36fad1-7d8d-41e8-8fad-768130fa1b0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@login_button = 'button_Login']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>login_button</name>
      <type>Main</type>
      <value>button_Login</value>
      <webElementGuid>7cd928d6-3544-4193-bd3a-33dc5b27865a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
