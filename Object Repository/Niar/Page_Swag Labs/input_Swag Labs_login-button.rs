<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Swag Labs_login-button</name>
   <tag></tag>
   <elementGuidId>6ab80c99-3cb5-4c47-8d85-3104c723d571</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='login-button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#login-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a9562f3a-5c0d-43ac-8c2e-22bc4aa518f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>e1f04b7f-cc99-4a7a-9ac0-ac770e5fcf7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-button btn_action</value>
      <webElementGuid>0ad27c6d-5a48-40bf-8e16-5e80e52c8dd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>dcdeef35-8c94-4a44-8625-d7fd35f4a7c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>8e316a2b-0abf-43bf-9e1e-028add92e0d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>574265b9-7bc0-47e8-af80-88ab17c4b70c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Login</value>
      <webElementGuid>1bd3b584-4a6b-4b86-ab81-6b67f9a47635</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login-button&quot;)</value>
      <webElementGuid>9b64b38b-0047-4698-9afd-5ee563d211e6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='login-button']</value>
      <webElementGuid>a58a322f-5c1f-4800-8cb0-34032bc7a656</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='login_button_container']/div/form/input</value>
      <webElementGuid>4f010a90-a8ca-4ae3-8d1a-f4f6e6776276</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/input</value>
      <webElementGuid>0847ee14-2290-40de-bd34-f54b55059687</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'login-button' and @name = 'login-button']</value>
      <webElementGuid>51ad4b5f-713a-43e7-8558-4be3a9eb1b6a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
