<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>username_field</name>
   <tag></tag>
   <elementGuidId>7efc969f-237e-4908-885b-957893f8bed7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'user-name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user-name</value>
      <webElementGuid>eb1da424-3708-4e8d-8ab0-9c04e423eeff</webElementGuid>
   </webElementProperties>
</WebElementEntity>
