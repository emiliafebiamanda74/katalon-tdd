<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Swag Labs_login-button</name>
   <tag></tag>
   <elementGuidId>b6301e07-f929-4bf7-a2fa-8d36f2dbbbdd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#login-button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='login-button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0cce5620-e4ce-47bb-aead-ec2fe3596bd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>2780bbcb-1cfb-4f42-a5a9-ab4796c47ec9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-button btn_action</value>
      <webElementGuid>98176e95-cc21-43d7-a7c1-876d58774b85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>faf80cda-87ee-4ab1-8887-d9a565f9acb8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>f2ab0257-f177-4519-b355-a9214d1694cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>bd92d8f1-9386-4034-9173-f9f96fe67b3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Login</value>
      <webElementGuid>504fb129-a224-4b90-a673-2d68542b70c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login-button&quot;)</value>
      <webElementGuid>303d2298-fc58-4fa2-be76-80bad4c6ff23</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='login-button']</value>
      <webElementGuid>87c0fedc-8797-4b0c-b471-cfc80de8ee66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='login_button_container']/div/form/input</value>
      <webElementGuid>7b1168a7-cf16-4ad5-aedb-a0ce58e3c981</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/input</value>
      <webElementGuid>c278c7a0-5352-430c-aa6d-97f5f5033d46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'login-button' and @name = 'login-button']</value>
      <webElementGuid>36e9754e-a634-49ac-bc9a-2fc2150c321b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
