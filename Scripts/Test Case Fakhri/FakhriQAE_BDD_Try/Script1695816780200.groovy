import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import java.io.File;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//Q4: yes, create an automation script using any framework of your choice.
//Using Selenium (Java) with Microsoft Edge Browser

public class CermatiGabungTest {
    public static void main(String[] args) throws InterruptedException{

    	File file = new File("C:/Selenium/edgedriver/msedgedriver.exe");
    	System.setProperty("webdriver.edge.driver", file.getAbsolutePath());
    	
    	
    	System.out.println("Execution after setting EdgeDriver path in System Variables on Windows!!");
        WebDriver driver = new EdgeDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
       
        
        driver.get("https://www.cermati.com/app/gabung");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h1[text()='Daftar Akun']")));
        fillFormFunction (driver, wait);
        Thread.sleep(3000);
        
    }
    
    
    public static void fillFormFunction (WebDriver driver, WebDriverWait wait) throws InterruptedException {
    	
    	WebElement emailInput = driver.findElement(By.id("email"));
    	WebElement phoneNumberInput = driver.findElement(By.id("mobilePhone"));
        WebElement passwordInput = driver.findElement(By.id("password")); 
        WebElement confirmPasswordInput = driver.findElement(By.id("confirmPassword"));
        WebElement firstNameInput = driver.findElement(By.id("firstName"));
        WebElement lastNameInput = driver.findElement(By.id("lastName"));
        WebElement cityProvinceInput = driver.findElement(By.id("cityAndProvince"));
        emailInput.sendKeys("fakhrinaufalzuhdi@gmail.com");
       	phoneNumberInput.sendKeys("081317278290");
        passwordInput.sendKeys("Cermati789");
       	confirmPasswordInput.sendKeys("Cermati789");
       	firstNameInput.sendKeys("Fakhri");
       	lastNameInput.sendKeys("Naufal Zuhdi");
       	cityProvinceInput.sendKeys("Kabupaten Tangerang");
       	Thread.sleep(2000);
       	WebElement firstItem = driver.findElement(By.xpath("//*[@class='autocomplete__icon-right_hrISI']"));
       	firstItem.click();
       	Thread.sleep(2000);
       	driver.findElement(By.xpath("//*[text()='Daftar']")).click();
       	System.out.println("Success Register to Cermati");
       
    }
    
   
    
    
}

